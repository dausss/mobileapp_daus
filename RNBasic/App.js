import React, { Component } from 'react';
import { AppRegistry, Text, View } from 'react-native';

class IDCard extends Component {
  render () {
    return (
        <View style={{alignItems:'center', backgroundColor:'brown'}}>
          <Text>Nama: {this.props.name}</Text>
          <Text>Kelas: {this.props.kelas}</Text>
          <Text>Zodiak: {this.props.zodiak}</Text>
        </View>
    );
  }
}

export default class ViewIDCard extends Component {
  render () {
    return (
        <View style={{alignItems: 'center'}}>
          <IDCard
          name='Daus :)'
          kelas='12 IPA 1'
          zodiak='Ikan'
          />
      </View>
    )
  }
}